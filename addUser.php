<?php
  require 'connectDB.php';
  $userName = htmlspecialchars($_POST[Name]);
  $userEmail = htmlspecialchars($_POST[Email]);
  $userRole = htmlspecialchars($_POST[Role]);
  $userStatus = htmlspecialchars($_POST[Status]);
  $stmt = $pdo->prepare('INSERT INTO user (Name, Email, Role, Status)  VALUES (?,?,?,?)');
  $stmt->execute(array($userName, $userEmail, $userRole, $userStatus));

?>
