<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Task 1</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="script.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  </head>
  <body>
    <div class="table-responsive container">
      <table class="table table-striped table-bordered table-hover">
        <thead>
          <tr class="info">
            <th><input type="checkbox" id="mainCheckBox" onclick="checkAll()"></th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Status</th>
            <th>Delete/Edit</th>
          </tr>
        </thead>
        <tbody id="tableBody">
        </tbody>
      </table>
    </div>


    <div class="container">
      <nav class="navbar navbar-default">
        <div class="row">
          <div class="col-md-4">
            <select id="dropMenu" class="form-control panelElements">
              <option selected disabled value="1">Select one...</option>
              <option value="2">Delete</option>
              <option value="3">Active</option>
              <option value="4">Not active</option>
            </select>
          </div>
          <div class="col-md-2">
            <input id="btnAction" type="submit" class="btn btn-info btn-block panelElements" value="Action" onclick="takeAction()">
          </div>
          <div class="col-md-3">
            <label id="errorAction" style="display:none" class="font-weight-bold text-danger">Помилка!</label>
          </div>
          <div class="col-md-2">
            <input id="btnAdd" type="submit" class="btn btn-info btn-block panelElements" value="Add" onclick="openAddModal()">
          </div>
        </div>
      </nav>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1">
      <div class="modal-dialog ">
        <div class="modal-content">
          <div class="modal-header">
            <button class="close" data-dismiss="modal">х</button>
            <h4 class="modal-title" value="0">Add new User</h4>
          </div>
          <div class="modal-body">
              <div class="inpArea">
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6 inpAreaName">
                      <input type="text" id="inpName" class="form-control" placeholder="Enter Name">
                    </div>
                    <div class="col-md-6 inpAreaEmail">
                      <input type="email" id="inpEmail" class="form-control " placeholder="Enter Email">
                    </div>
                  </div>
                </div>
              </div>
            <div class="row">
              <div class="col-md-6">
                <select id="selRole" class="form-control" value="">
                  <option selected disabled>Select role...</option>
                  <option value="2">Admin</option>
                  <option value="3">Manager</option>
                </select>
              </div>
              <div class="col-md-6">
                <select id="selStatus" class="form-control ">
                  <option selected disabled>Select status...</option>
                  <option value="2">Active</option>
                  <option value="3">Not active</option>
                </select>
              </div>
            </div>
            <label id="errorAddUser" style="display:none" class="font-weight-bold text-danger">Помилка!</label>
          </div>
          <div class="modal-footer">
            <button id="btnModalAdd" class="btn btn-success" onclick="addNewUser()">Add user</button>
            <button id="btnModalSave" style="display:none" class="btn btn-success" onclick="editUserInformation()">Save</button>
          </div>
        </div>
      </div>
    </div>




    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </body>
</html>
