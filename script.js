document.addEventListener('DOMContentLoaded', function(){
    showTable();
});
function showTable() {
  $.post("check.php", {}, function (data){
      data = JSON.parse(data);

      $("#tableBody").empty();
      $("#mainCheckBox").prop('checked', false);
      if (data) {
        for (let i = 0; i < data.length; i++) {
          $("<tr id='TR"+data[i]['id']+"'><th><input type='checkbox' value='"+data[i]['id']+"' onclick='checkMain()'></th><th>"+data[i]['Name']+"</th><th>"+data[i]['Email']+"</th><th>"+data[i]['Role']+"</th><th class='statusColumn'><input class="+data[i]['Status']+" type='checkbox' onclick='checkedStutus("+data[i]['id']+")'></th><th class='iconColumn'><img class='imgDelete' src='image/delete.png' onclick='funcIconDelete("+data[i]['id']+")'><img class='imgEdit' src='image/edit.png' onclick='funcIconEdit("+data[i]['id']+")'></th></tr>").appendTo("#tableBody");
        }
      }
    }
  )
}
function addNewUser() {
  let strNameUser = $("#inpName").val();
  let strEmailUser = $("#inpEmail").val();
  let strRoleUser = $("#selRole :selected").html();
  let strStatusUser;
  if ($("#selStatus :selected").html() == "Not active") {
    strStatusUser = "NotActive";
  }else if ($("#selStatus :selected").html() == "Active") {
    strStatusUser = "Active";
  }
  let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if($.trim(strNameUser) == "") {
    $("#errorAddUser").html("Enter correct name!");
    $("#errorAddUser").show();
  } else if (reg.test(strEmailUser) == false) {
    $("#errorAddUser").html("Enter correct Email!");
    $("#errorAddUser").show();
  } else  if ($("#selRole :selected").html() == "Select role...") {
    $("#errorAddUser").html("Select correct Role!");
    $("#errorAddUser").show();
  } else if ($("#selStatus :selected").html() == "Select status...") {
    $("#errorAddUser").html("Select correct Status!");
    $("#errorAddUser").show();
  } else {
    $.ajax({
      url: "addUser.php",
      type: "POST",
      data: ({Name : strNameUser, Email : strEmailUser, Role : strRoleUser, Status : strStatusUser}),
      dataType: "html",
      beforeSend: funcBefore,
      success: funcSuccess
    })
  }
}
function funcBefore() {
  $("#errorAddUser").html("Please wait...");
  $("#errorAddUser").show();
}
function funcSuccess() {
  $("#errorAddUser").hide();
  showTable();
  $("#myModal").modal("hide");
}
function takeAction() {
  var checkboxes = [];
  $('input:checkbox:checked[value]').each(function(){
  checkboxes.push(this.value);
  });
  if (checkboxes.length > 0) {
    if ($("#dropMenu :selected").html() == "Delete") {
      $("#errorAction").hide();
      if (confirm("Deleted selected rows?")) {
        $("#errorAction").html("Please wait...");
        $("#errorAction").show();
        $.post("deleteUser.php", {'checkboxes[]':checkboxes}, function (data){
            showTable();
            $("#errorAction").hide();
          })
      }
    }
    else if ($("#dropMenu :selected").html() == "Active") {
      $("#errorAction").hide();
      $("#errorAction").html("Please wait...");
      $("#errorAction").show();
      $.post("activeUser.php", {'checkboxes[]':checkboxes}, function (data){
          showTable();
          $("#errorAction").hide();
        })
    }
    else if ($("#dropMenu :selected").html() == "Not active") {
      $("#errorAction").hide();
      $("#errorAction").html("Please wait...");
      $("#errorAction").show();
      $.post("notActiveUser.php", {'checkboxes[]':checkboxes}, function (data){
          showTable();
          $("#errorAction").hide();
        })
    }
    else {
      $("#errorAction").html("Select one action!");
      $("#errorAction").show();
    }
  } else {
    $("#errorAction").html("Select one or more rows!");
    $("#errorAction").show();
  }
}
function checkAll() {
  if (($("#mainCheckBox").prop('checked')) != true) {
    $("input[type='checkbox']").prop('checked', false);
  } else {
    $("input[type='checkbox']").prop('checked', true);
  }
}
function checkMain() {
  $("#mainCheckBox").prop('checked', false);
}
function funcIconDelete(id) {
  // alert(id);
  var checkboxes = [];
  checkboxes.push(id);
  $("#errorAction").hide();
  if (confirm("Deleted selected rows?")) {
    $("#errorAction").html("Please wait...");
    $("#errorAction").show();
    $.post("deleteUser.php", {'checkboxes[]':checkboxes}, function (data){
        showTable();
        $("#errorAction").hide();
      })
  }
}
function funcIconEdit(id) {
  $("#errorAddUser").hide();
  $(".modal-title").text("Edit information:");
  $(".modal-title").val(id);
  $("#btnModalAdd").hide();
  $("#btnModalSave").show();
  let idRow = "TR"+id;

  $("#inpName").val($("#"+idRow).children('th').eq(1).html());
  $("#inpEmail").val($("#"+idRow).children('th').eq(2).html());
  $("#myModal").modal("show");
}
function editUserInformation() {
  let strNameUser = $("#inpName").val();
  let strEmailUser = $("#inpEmail").val();
  let strRoleUser = $("#selRole :selected").html();
  let strIdUser = $(".modal-title").val();
  if ($("#selStatus :selected").html() == "Not active") {
    strStatusUser = "NotActive";
  }else if ($("#selStatus :selected").html() == "Active") {
    strStatusUser = "Active";
  }

  let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if($.trim(strNameUser) == "") {
    $("#errorAddUser").html("Enter correct name!");
    $("#errorAddUser").show();
  } else if (reg.test(strEmailUser) == false) {
    $("#errorAddUser").html("Enter correct Email!");
    $("#errorAddUser").show();
  } else  if ($("#selRole :selected").html() == "Select role...") {
    $("#errorAddUser").html("Select correct Role!");
    $("#errorAddUser").show();
  } else if ($("#selStatus :selected").html() == "Select status...") {
    $("#errorAddUser").html("Select correct Status!");
    $("#errorAddUser").show();
  } else {
    if (confirm("Edit selected rows?")) {
      $.ajax({
        url: "updateUser.php",
        type: "POST",
        data: ({id : strIdUser, Name : strNameUser, Email : strEmailUser, Role : strRoleUser, Status : strStatusUser}),
        dataType: "html",
        beforeSend: funcBefore,
        success: funcSuccess
      })
    }
  }
}
function openAddModal() {
  $("#errorAddUser").hide();
  $(".modal-title").text("Add new User");
  $("#btnModalAdd").show();
  $("#btnModalSave").hide();
  $("#inpName").val("");
  $("#inpEmail").val("");

  $("#myModal").modal("show");
}
function checkedStutus(id){
  let idRow = "TR"+id;
  // let userStatus;
  let userStatus = $("#"+idRow).children('th').eq(4).children('input').attr('class');
  if (userStatus == 'Active') {
    var userNewStatus = 'NotActive';
    $("#"+idRow).children('th').eq(4).children('input').attr('class', 'NotActive');
  } else {
    var userNewStatus = 'Active';
    $("#"+idRow).children('th').eq(4).children('input').attr('class', 'Active');
  }
  $.ajax({
    url: "checkStatus.php",
    type: "POST",
    data: ({userStatus : userNewStatus, userId : id}),
    dataType: "html"
  })
}
